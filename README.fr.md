# CHALLENGE ECOLE 42

---
Je ne suis pas une étudiante de l'école 42, mais j'ai décidé de me challenger sur leurs différents projets.

---
# Sommaire du repository

## 1 - La Piscine
* [C00](piscine/C00) - [EN](piscine/C00/README.md) - [FR](piscine/C00/README.fr.md)
* [C01](piscine/C01) - [EN](piscine/C01/README.md) - [FR](piscine/C01/README.fr.md)

## 2 - Tron commun
* [Net_practice](core/net_practice) - [EN](core/net_practice/README.md) - [FR](core/net_practice/README.fr.md)

---
# Outils

* [Francinette - Paco](https://github.com/xicodomingues/francinette/tree/master/tests/cpiscine/c00/ex07)
* [Mini norminette](https://github.com/khairulhaaziq/mini-moulinette)
* [Python Tutor](https://pythontutor.com/c.html#mode=edit)
