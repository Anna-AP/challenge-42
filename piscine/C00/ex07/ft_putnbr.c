/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expend_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anna-ap <anna-ap@mail.com>                 +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/16 18:40:30 by anna-ap           #+#    #+#             */
/*   Updated: 2024/03/16 18:40:30 by anna-ap          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_count(long value)
{
	int	i;

	i = 0;
	while (value != 0)
	{
		value = value / 10;
		i++;
	}
	return (i);
}

int	ft_int(int v)
{
	int	k;

	k = v;
	while (k / 10)
		k = k / 10;
	return (k);
}

int	ft_m(int v, int count)
{
	while (count > 0)
	{
		v = v * 10;
		count--;
	}
	return (v);
}

void	ft_write(int count, long nb2)
{
	int	i;
	int	l;
	int	m;

	while (count > 0 && nb2 != -2147483648)
	{
		i = ft_int(nb2);
		l = i + 48;
		write(1, &l, 1);
		m = ft_m(i, count -1);
		nb2 = nb2 - m;
		count--;
	}
}

void	ft_putnbr(int nb)
{
	long	nb2;
	int		count;

	nb2 = nb;
	count = ft_count(nb2);
	if (nb2 == 0)
		write(1, "0", 1);
	if (nb == -2147483648)
		write (1, "-2147483648", 11);
	if (nb2 < 0 && nb2 != -2147483648)
	{
		write(1, "-", 1);
		nb2 = nb2 * -1;
	}
	ft_write(count, nb2);
}

/*int main()
{
	ft_putnbr(-123);

    return 0;
}*/
