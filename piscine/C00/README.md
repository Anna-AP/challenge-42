# THE PISCINE - C00

---
**Other language :**
* [French](README.fr.md)

---
[Video lessons (fr)](https://www.youtube.com/playlist?list=PLVQYiy6xNUxz5wbzZn4tfUhF4djgzscB-)
* [C - I Hello World](https://youtu.be/1RuoZeKXWQY?si=_5bUvgNRaXCJPgCn)
* [C - II Variables - 1 Introduction](https://youtu.be/g1Yv7quz9jw?si=fhP5rEO4BhPcazeP)
* [C - II Variables - 2 Binary](https://youtu.be/I5UlJXNEKXY?si=5bQ-3392gSnuEb23)
* [C - II Variables - 3 Types of variables](https://youtu.be/1S8lV9pqrxs?si=gLPwfCiXYp1d09fa)
* [C - II Variables - 4 Scopes](https://youtu.be/Iw-WgHy-4Y4?si=eK1hlV9bJHpphsE8)
* [C - II Variables - 5 The statics array](https://youtu.be/oUcsC7i__8c?si=TT10S5G2VkFWk_O1)
* [C - II Variables - 6 Stack](https://youtu.be/CPQVaU8pjbU?si=M05BUYPGJS6eI07p)
* [C - III Les opérateurs - 1 Introduction](https://youtu.be/JIivicjwF48?si=OJuDO4wlv9w0oqLj)
* [C - III Les opérateurs - 2 Incrementing and decrementing](https://youtu.be/-Aiuupzifjc?si=bm1pDlLHtv51HLsb)
* [C - III Les opérateurs - 3 Binary operators](https://youtu.be/yQ70FEEzjPM?si=M3bMP9H07k_85mkH)
* [C - III Les opérateurs - 4 Comparison operators](https://youtu.be/UWRcvIRu_XQ?si=39aj4Cs262wbonHr)
* [C - IV Conditionnelles - 1 if, else et else if](https://youtu.be/jIwybdfnmA0?si=V6gvYl24lEDckFAu)
* [C - IV Conditionnelles - 2 switch et break](https://youtu.be/BJZvfuCgaUA?si=vO3XA1KAA2QZQPAG)
* [C - IV Conditionnelles - 3 ternary](https://youtu.be/l0svMuRYf8o?si=vFVgB8VYnSUN0AM_)
* [C - V Les boucles - 1 while](https://youtu.be/j6Z4J0Q4s6g?si=hwQPSXd42TSqApdz)
* [C - V Les boucles - 2 do while](https://youtu.be/3UuxaZfgC_I?si=1YOLD8VEt_0XtGaa)
* [C - V Les boucles - 3 for](https://youtu.be/uvLGuAXQtUY?si=1XP6T5plgL-qGAVH)
* [C - V Les boucles - 4 break and continue](https://youtu.be/QTSG5bGFMhM?si=Y9M09v9HgJvIvEWC)
* [C - V Les boucles - 5 goto and label](https://youtu.be/mSs4wrlXGLY?si=ssZ3e-uTqrwPlcr1)
* [C - VI Fonctions - 1 Introduction](https://youtu.be/whA0Q0tL9bs?si=2EcVzKMkJ8OJ_EjY)
* [C - VI Fonctions - 2 Function prototype](https://youtu.be/SYSTsWANGxY?si=YuGQbuS1u1JmBt0S)
* [C - VI Fonctions - 3 return](https://youtu.be/ylbDfQAzIUQ?si=gFPCcWdArZHwqajb)
* [C - VI Fonctions - 4 The arguments](https://youtu.be/PDIIN3xqaQI?si=btJGlTT5EwuekUki)
* [C - XI Autres mots clés - static](https://youtu.be/Yw_lNNVFhmk?si=W_kQuaqAJrnAp6is)
* [C - XI Autres mots clés - const](https://youtu.be/Z_iXk2qysm8?si=IWAMoHBBToeAzwCj)
