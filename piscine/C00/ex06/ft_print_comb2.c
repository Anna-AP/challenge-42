/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expend_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anna-ap <anna-ap@mail.com>                 +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/16 18:40:30 by anna-ap           #+#    #+#             */
/*   Updated: 2024/03/16 18:40:30 by anna-ap          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print(int i, int j)
{
	int	k;
	int	l;
	int	m;
	int	n;

	k = i / 10 + '0';
	l = i % 10 + '0';
	m = j / 10 + '0';
	n = j % 10 + '0';
	write(1, &k, 1);
	write(1, &l, 1);
	write(1, " ", 1);
	write(1, &m, 1);
	write(1, &n, 1);
	if (i != 98 || j != 99)
		write(1, ", ", 2);
}

void	ft_print_comb2(void)
{
	int	i;
	int	j;

	i = 0;
	j = 1;
	while (i < 99)
	{
		ft_print(i, j);
		j++;
		if (j == 100)
		{
			i++;
			j = i + 1;
		}
	}
}

/*int main(void)
{
     ft_print_comb2();
}*/
