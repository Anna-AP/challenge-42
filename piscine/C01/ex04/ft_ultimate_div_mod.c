/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expend_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anna-ap <anna-ap@mail.com>                 +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/16 18:40:30 by anna-ap           #+#    #+#             */
/*   Updated: 2024/03/16 18:40:30 by anna-ap          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

void	ft_ultimate_div_mod(int *a, int *b)
{
	int	div;
	int	mod;

	div = *a / *b;
	mod = *a % *b;
	*a = div;
	*b = mod;
}

/*int main()
{
	int	a;
	int	b;

	a = 11;
	b = 2;

	ft_div_mod(a, b);
	printf("div : %d et mod : %d \n", div, mod);

	return 0;
}*/
