/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expend_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anna-ap <anna-ap@mail.com>                 +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/16 18:40:30 by anna-ap           #+#    #+#             */
/*   Updated: 2024/03/16 18:40:30 by anna-ap          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*#include <stdio.h>*/

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		i++;
	}
	return (i);
}

/*int	main(void)
{
	char	*str;
	int		len;

	str = "Bonjour le monde!";
	len = ft_strlen("Bonjour le monde!");
	printf("%d\n", len);
	return (0);
}
*/
