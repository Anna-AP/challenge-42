# LA PISCINE - C01

---
**Autre langue :**
* [Anglais](README.md)

---
[Leçons vidéos](https://www.youtube.com/playlist?list=PLVQYiy6xNUxytsXWxZx6odBJMbRktIHTs)
* [C - VII Pointeurs - 1 Introduction](https://youtu.be/iooNvEbpqsQ?si=MSrVvW-UaB3Lk9Pw)
* [C - VII Pointeurs - 2 Assignation](https://youtu.be/B1pPi0d0uZc?si=FMcG4Un552pE3O9_)
* [C - VII Pointeurs - 3 Déréférencement](https://youtu.be/4EWDazdBW78?si=-Dr2k0uo82Ur_HYD)
* [C - VII Pointeurs - 4 Arithmétique des pointeurs](https://youtu.be/bKuGUoKv8Oo?si=q9-0Up4dyFC_V4Gx)
* [C - VII Pointeurs - 5 Les tableaux](https://youtu.be/YNu26XdEJDs?si=3xbZK4ubBzvN8pUn)
* [C - VII Pointeurs - 6 Les chaines de caractère](https://youtu.be/SeU8JQ-sFwI?si=TNXdBNaJNUlK0LS6)
* [C - VII Pointeurs - 7 Utilisation générale](https://youtu.be/FWVHI0sqeVE?si=SnqE-r2JSf9oUJ-I)
* [C - VII Pointeurs - 8 void](https://youtu.be/GxJ6_emGGdo?si=O5zSe32RA6yDV-gP)
