/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expend_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anna-ap <anna-ap@mail.com>                 +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/16 18:40:30 by anna-ap           #+#    #+#             */
/*   Updated: 2024/03/16 18:40:30 by anna-ap          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

void	ft_swap(int *a, int *b)
{
	int	ptrc;

	ptrc = *a;
	*a = *b;
	*b = ptrc;
}

/*int main()
{
	int	a;
	int	b;
	int *ptra;
	int *ptrb;

	a = 2;
	b = 3;
	ptra = &a;
	ptrb = &b;

	printf("a : %d et b : %d \n", a, b);
	ft_swap(ptra, ptrb);
	printf("a : %d et b : %d \n", a, b);

	return 0;
}*/
