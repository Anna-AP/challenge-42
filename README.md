# CHALLENGE 42 SCHOOL

---
I'm not a student of the 42 school, but I decided to challenge myself on their various projects.

---
# Repository summary

## 1 - The Piscine
* [C00](piscine/C00) - [EN](piscine/C00/README.md) - [FR](piscine/C00/README.fr.md)
* [C01](piscine/C01) - [EN](piscine/C01/README.md) - [FR](piscine/C01/README.fr.md)

## 2 - Core
* [Net_practice](core/net_practice) - [EN](core/net_practice/README.md) - [FR](core/net_practice/README.fr.md)

---
# Tools

* [Francinette - Paco](https://github.com/xicodomingues/francinette/tree/master/tests/cpiscine/c00/ex07)
* [Mini norminette](https://github.com/khairulhaaziq/mini-moulinette)
* [Python Tutor](https://pythontutor.com/c.html#mode=edit)
